package app.Models;

import app.domain.Practice;
import app.domain.User;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

import java.util.ArrayList;
import java.util.List;

public class Orden {

    private SimpleStringProperty id = new SimpleStringProperty();
    private SimpleObjectProperty<Practice> practice = new SimpleObjectProperty<>();
    private SimpleObjectProperty<app.domain.User> user = new SimpleObjectProperty<>();
    private SimpleLongProperty reqDate = new SimpleLongProperty();
    private SimpleObjectProperty<List<Material>> materials = new SimpleObjectProperty<>();

    public Practice getPractice() {
        return practice.get();
    }

    public SimpleObjectProperty<Practice> practiceProperty() {
        return practice;
    }

    public void setPractice(final Practice practice) {
        this.practice.set(practice);
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public app.domain.User getUser() {
        return user.get();
    }

    public SimpleObjectProperty<app.domain.User> userProperty() {
        return user;
    }

    public void setUser(final app.domain.User user) {
        this.user.set(user);
    }

    public long getReqDate() {
        return reqDate.get();
    }

    public SimpleLongProperty reqDateProperty() {
        return reqDate;
    }

    public void setReqDate(long reqDate) {
        this.reqDate.set(reqDate);
    }

    public List<Material> getMaterials() {
        return materials.get();
    }

    public SimpleObjectProperty<List<Material>> materialsProperty() {
        return materials;
    }

    public void setMaterials(List<Material> materials) {
        this.materials.set(materials);
    }

    @Override
    public String toString() {
        return "Orden{" +
                "id=" + id.get() +
                ", practice=" + practice.get() +
                ", user=" + user.get() +
                ", reqDate=" + reqDate.getName() +
                ", materials=" + materials.get() +
                '}';
    }

    public static Orden fromJson(final JsonObject orderJson) {
        final Orden orden = new Orden();
        orden.id.set(orderJson.get("_id").getAsString());
        orden.reqDate.set(orderJson.get("reqDate").getAsLong());


        final JsonElement jsonPractice = orderJson.get("practice");
        if (jsonPractice != null && !jsonPractice.toString().equals("null")) {
            final Practice practice = Practice.fromJson(jsonPractice.getAsJsonObject());
            orden.setPractice(practice);
        }

        final User user = User.fromJson(orderJson.get("user").getAsJsonObject());
        orden.setUser(user);

        // final JsonArray jsonMaterials = orderJson.getAsJsonArray("practice").getAsJsonArray();
        final JsonArray jsonMaterials = orderJson.getAsJsonObject("practice").getAsJsonArray("material");
        final List<Material> materials = new ArrayList<>();
        jsonMaterials.forEach(jsonElement -> {
            final Material material = Material.fromJson(jsonElement.getAsJsonObject());
            materials.add(material);
        });
        orden.setMaterials(materials);

        System.out.println(materials.size());

        return orden;
    }
    
}
