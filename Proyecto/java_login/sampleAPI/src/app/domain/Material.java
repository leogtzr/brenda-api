package app.domain;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Material {

    @SerializedName("_id")
    @Expose
    private String _id;

    private String name;
    private String type;
    private int stock;
    private String image;
    private String description;
    private String capacity;

    public String getId() {
        return _id;
    }

    public void setId(String id) {
        this._id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        return "Material{" +
                "id='" + _id + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", stock=" + stock +
                ", image='" + image + '\'' +
                ", description='" + description + '\'' +
                ", capacity='" + capacity + '\'' +
                '}';
    }

    /*
    "material": [
    {
        "_id": "5c85b5e8a30f0836a1ad2cab",
        "name": "Matraz Aforado",
        "type": "Vidrio",
        "stock": 30,
        "image": null,
        "description": null,
        "capacity": "500 ml",
        "__v": 0
    },
     */

    public static Material fromJson(final JsonObject jsonObject) {

        final Material material = new Gson().fromJson(jsonObject, Material.class);
//        final Material material = new Material();
//        material.setId(jsonObject.get("_id").getAsString());
//        material.setName(jsonObject.get("name").getAsString());
//        material.setType(jsonObject.get("type").getAsString());
//        material.setStock(jsonObject.get("stock").getAsInt());
//
//        final JsonElement image = jsonObject.get("image");
//        if (image != null && !image.toString().equals("null")) {
//            material.setImage(image.getAsString());
//        }
//
//        final JsonElement description = jsonObject.get("description");
//        if (description != null && !description.toString().equals("null")) {
//            material.setDescription(description.getAsString());
//        }
//        material.setCapacity(jsonObject.get("capacity").getAsString());

        // System.out.println(material);

        return material;
    }

}
