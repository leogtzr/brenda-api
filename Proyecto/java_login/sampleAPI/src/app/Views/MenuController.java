package app.Views;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
// import javafx.scene.control.Button;

public class MenuController implements Initializable {

//    @FXML
//    private Button pedidosBtn;
//
//    @FXML
//    private Button inventarioBtn;

    @FXML
    private void openPedidos() {
        final Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("./Ordenes.fxml"));
            final Stage primaryStage = new Stage();
            configureModalWindow(primaryStage, "Pedidos", root);

            primaryStage.show();
        } catch (final IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void openInventario() {
        final Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("./Materiales.fxml"));
            final Stage primaryStage = new Stage();
            configureModalWindow(primaryStage, "Inventario", root);

            primaryStage.show();
        } catch (final IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {

    }

    private void configureModalWindow(final Stage stage, final String windowTitle, final Parent root) {
        stage.setTitle(windowTitle);
        stage.setScene(new Scene(root));
        stage.initModality(Modality.APPLICATION_MODAL);
    }

}
