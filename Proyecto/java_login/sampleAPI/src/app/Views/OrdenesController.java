package app.Views;

import app.Models.Orden;
import app.Services.Global;
import app.Services.OrdenService;
import app.domain.Material;
import com.google.gson.JsonObject;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class OrdenesController implements Initializable {

    private OrdenService ordenService = new OrdenService();

    @FXML
    private Button markAsDoneBt;

    @FXML
    private TextField alumno;

    @FXML
    private TextField practica;

    @FXML
    private TextField fechaRequisicion;

    @FXML
    private TableView<Orden> tablaOrdenes;

    @FXML
    private TextArea areaMateriales;

    @FXML
    private TableColumn user;

    @FXML
    private TableColumn practice;

    private ObservableList<Orden> ordenes;

    private int posicionOrdenEnTabla;

    @FXML
    private void markOrderAsDone(final ActionEvent event) {
        final Orden orden = tablaOrdenes.getSelectionModel().getSelectedItem();
        if (orden == null) {

            final Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Seleccione una orden");
            alert.setHeaderText("Por favor seleccione una orden para proceder.");

            alert.showAndWait().ifPresent(rs -> {
                if (rs == ButtonType.OK) {
                    System.out.println("Pressed OK.");
                }
            });
            return;
        }

        final Alert ordenRemovalConfirmationAlert = new Alert(Alert.AlertType.CONFIRMATION);
        ordenRemovalConfirmationAlert.setTitle("Eliminar Orden");
        ordenRemovalConfirmationAlert.setHeaderText("Desea eliminar/marcar como realizada la orden: " + orden.getId());
        ordenRemovalConfirmationAlert.setContentText("Desea eliminar la orden?");

        final Optional<ButtonType> result = ordenRemovalConfirmationAlert.showAndWait();
        if (result.isPresent()) {
            final ButtonType confirmationType = result.get();
            if (confirmationType == ButtonType.OK) {
                System.out.println("Ok ... proceeding to remove the order ... ");

                final Optional<JsonObject> jsonObject = ordenService.markOrderAsDone(Global.token, orden.getId());
                if (jsonObject.isPresent()) {
                    System.out.println("Response found ... ");
                    System.out.println(jsonObject.get());

                    final int pos = posicionOrdenEnTabla;
                    ordenes.remove(pos);
                    tablaOrdenes.refresh();
                    areaMateriales.setText("");
                }
            }
        }

    }


    private final ListChangeListener<Orden> selectorTablaOrdenes = c -> ponerOrdenSeleccionado();

    private Orden getTablaOrdenSeleccionado() {
        if (tablaOrdenes != null) {
            final List<Orden> orders = tablaOrdenes.getSelectionModel().getSelectedItems();
            if (orders != null && !orders.isEmpty() && orders.size() == 1) {
                final Orden competicionSeleccionada = orders.get(0);
                return competicionSeleccionada;
            }
        }
        return  null;
    }

    private void ponerOrdenSeleccionado() {
        final Orden orden = getTablaOrdenSeleccionado();
        posicionOrdenEnTabla = ordenes.indexOf(orden);

        if (orden != null) {
            markAsDoneBt.setDisable(false);
        }
    }

    private void inicializarTablaOrdenes() {
        user.setCellValueFactory(new PropertyValueFactory<Orden, Object>("user"));
        practice.setCellValueFactory(new PropertyValueFactory<Orden, Object>("practice"));

        ordenes = FXCollections.observableArrayList();
        tablaOrdenes.setItems(ordenes);

        System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        System.out.println(Global.token);
        System.out.println(Global.identity);
        System.out.println("vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv");
        // TODO: Initialize here ... remove it when done.
//      Global.token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1Yzg1YjMwZDBjYjYyNzE5NDk5YTk1YTgiLCJ1c2VybmFtZSI6ImxlbyIsInBhc3N3b3JkIjoiJDJiJDEwJFRubGN4REtkWXJDZDRTeDdUVzdNVmU3THZVRE1ZVWd3NjRhZkdsaFVGMEJKclduZHpjQk5tIiwibmFtZSI6ImxlbyIsInN1cm5hbWUiOiJndHpyIiwiZ3JhZGUiOjEsImltYWdlIjpudWxsLCJpYXQiOjE1NTMxODQxNDksImV4cCI6MTU1MzI3MDU0OX0.sU1lKevGQdMCK6fYxoLrb8WGkjvZCX9S18-d8LMUKxQ";
        // final String userId = Global.userId;
        final String userId = "5c85b30d0cb62719499a95a8";

        ordenService.fillUpOrdersByUser(ordenes, Global.token, userId);

        tablaOrdenes.refresh();

    }

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        this.inicializarTablaOrdenes();
        final ObservableList<Orden> tablaMateriaSel = tablaOrdenes.getSelectionModel().getSelectedItems();
        tablaMateriaSel.addListener(selectorTablaOrdenes);
    }

    @FXML
    private void ordenSelected(final MouseEvent click) {
        final Orden orden = tablaOrdenes.getSelectionModel().getSelectedItem();
        if (orden != null) {
            alumno.setText(orden.getUser().toString());
            if (orden.getPractice() != null) {
                practica.setText(orden.getPractice().toString());
                areaMateriales.setText(formatMaterialsToText(orden.getPractice().getMateriales()));
            }

            // Code to set fecha de requisicion:
            final Date date = new Date();
            final long unixTime = orden.getReqDate();
            date.setTime(unixTime * 1_000);
            fechaRequisicion.setText(date.toString());

            // TODO: remove this:
            System.out.println("~~~~~~~~~~~~~~~ Selected ~~~~~~~~~~~~~~~~~~");
            System.out.println(orden);
            System.out.println("~~~~~~~~~~~~~~~ Selected ~~~~~~~~~~~~~~~~~~");
        }
    }

    private String formatMaterialsToText(final List<Material> materials) {
        final StringBuilder sb = new StringBuilder();

        materials.forEach(material -> {
            sb.append(material.getName()).append(", ").append(", stock: ").append(material.getStock());
            if (material.getDescription() != null && !material.getDescription().equals("null")) {
                sb.append(material.getDescription());
            }
            sb.append("\r\n");
        });

        return sb.toString();
    }

}