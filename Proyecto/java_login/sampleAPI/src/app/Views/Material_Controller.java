package app.Views;

import app.Models.Material;
import app.Services.Global;
import app.Services.MaterialService;
import app.domain.MaterialDTO;
import com.google.gson.JsonObject;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class Material_Controller implements Initializable {

    //String ruta = Global.BASE_URI + "material";
    MaterialService materialService = new MaterialService();
    // ArrayList<Material> lisMateriales = new ArrayList<>();
    // ObservableList<Material> lista = FXCollections.observableArrayList();

    @FXML
    private Button anadirbt;
    @FXML
    private Button modificarbt;
    @FXML
    private Button eliminarbt;
    @FXML
    private Button nuevobt;

    @FXML
    private TextField materialTF;
    @FXML
    private TextField capacidadTF;
    @FXML
    private TextField presentacionTF;
    @FXML
    private TextField existenciaTF;

    @FXML
    private TableView<Material> tablamateriales;
    @FXML
    private TableColumn columaterial;
    @FXML
    private TableColumn colucapacidad;
    @FXML
    private TableColumn colupresentacion;
    @FXML
    private TableColumn coluexistencia;

    ObservableList<Material> materiales;


    private int posicionMaterialEnTabla;

    @FXML
    private void nuevo(final ActionEvent event) {
        clearComponents();
        modificarbt.setDisable(true);
        eliminarbt.setDisable(true);
        anadirbt.setDisable(false);
    }

    private void clearComponents() {
        materialTF.setText("");
        capacidadTF.setText("");
        presentacionTF.setText("");
        existenciaTF.setText("");
    }

    @FXML
    private void anadir(final ActionEvent event) {

        System.out.println("Trying to add a new material ... ");

        final String materialName = materialTF.getText();
        final String capacidad = capacidadTF.getText();
        final String presentacion = presentacionTF.getText();
        final String existencia = existenciaTF.getText();

        materialService.makeMat(materialName, capacidad, presentacion, existencia);
        final MaterialDTO dto = new MaterialDTO.Builder()
                .materialName(materialName)
                .capacidad(capacidad)
                .presentacion(presentacion)
                .existencia(existencia)
                .build()
                ;

        System.out.println(dto);

        Material material = new Material();
        material.material.set(materialName);
        material.capacidad.set(capacidad);
        material.presentacion.set(presentacion);
        material.existencia.set(existencia);

        materiales.add(material);

        System.out.println("Done ... ");

        final Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(String.format("Material '%s' añadido correctamente.", materialName));
        alert.setHeaderText("Material añadido.");

        alert.showAndWait().ifPresent(rs -> {
            if (rs == ButtonType.OK) {
                // System.out.println("Pressed OK.");
            }
        });

        clearComponents();
    }

    @FXML
    private void modificar(final ActionEvent event) {

        System.out.printf("Modificar.index is: %d\n", this.posicionMaterialEnTabla);
        final Material selectedMaterial = tablamateriales.getSelectionModel().getSelectedItem();
        if (selectedMaterial == null) {
            final Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Seleccione un material");
            alert.setHeaderText("Seleccione un material de la tabla.");
            alert.showAndWait().ifPresent(rs -> {
                if (rs == ButtonType.OK) {
                    //
                }
            });
            return;
        }

        final Material material = new Material();
        material.setId(selectedMaterial.getId());
        material.material.set(materialTF.getText());
        material.capacidad.set(capacidadTF.getText());
        material.presentacion.set(presentacionTF.getText());
        material.existencia.set(existenciaTF.getText());
        materiales.set(this.posicionMaterialEnTabla, material);

        System.out.println("To edit: begin");
        System.out.println(material);
        System.out.println("To edit: end");

        final Optional<JsonObject> editMaterialResponse = materialService.editMaterial(
                Global.token
                , material.getId()
                , material.getMaterial()
                , material.getPresentacion()
                , material.getExistencia()
                , material.getCapacidad()
        );
        System.out.println(editMaterialResponse);
        if (editMaterialResponse.isPresent()) {
            final JsonObject updatedMaterialJson = editMaterialResponse.get();
            System.out.println(updatedMaterialJson);
        }

    }


    @FXML
    private void eliminar(final ActionEvent event) {
        final int pos = this.posicionMaterialEnTabla;
        final Material materialToDelete = materiales.get(pos);

        System.out.println("About to remove: " + materialToDelete.getId());
        materialService.remove(materialToDelete.getId());
        materiales.remove(pos);
        tablamateriales.refresh();
    }

    @FXML
    private void materialSelected(final MouseEvent click) {
        final Material material = tablamateriales.getSelectionModel().getSelectedItem();

        this.posicionMaterialEnTabla = tablamateriales.getSelectionModel().getSelectedIndex();
        System.out.printf("Index is: %d\n", this.posicionMaterialEnTabla);

        if (material != null) {
            System.out.println("~~~~~~~~~~~~~~~ Selected ~~~~~~~~~~~~~~~~~~");
            System.out.println(material);
            System.out.println("~~~~~~~~~~~~~~~ Selected ~~~~~~~~~~~~~~~~~~");
        }
    }

    private final ListChangeListener<Material> selectorTablaMateriales = c -> ponerMaterialSeleccionado();

    private Material getTablaMaterialSeleccionado() {
        if (tablamateriales != null) {
            final List<Material> materials = tablamateriales.getSelectionModel().getSelectedItems();
            if (materials != null && !materials.isEmpty() && materials.size() == 1) {
                final Material competicionSeleccionada = materials.get(0);
                return competicionSeleccionada;
            }
        }
        return  null;
    }

    private void ponerMaterialSeleccionado() {
        final Material material = getTablaMaterialSeleccionado();
        posicionMaterialEnTabla = materiales.indexOf(material);

        if (material != null) {
            materialTF.setText(material.getMaterial());
            capacidadTF.setText(material.getCapacidad());
            presentacionTF.setText(material.getPresentacion());
            existenciaTF.setText(material.getExistencia());

            modificarbt.setDisable(false);
            eliminarbt.setDisable(false);
            anadirbt.setDisable(true);
        }
    }

    private void inicializarTablaMateriales() {

        columaterial.setCellValueFactory(new PropertyValueFactory<Material,String>("material"));
        colucapacidad.setCellValueFactory(new PropertyValueFactory<Material,String>("capacidad"));
        colupresentacion.setCellValueFactory(new PropertyValueFactory<Material,String>("presentacion"));
        coluexistencia.setCellValueFactory(new PropertyValueFactory<Material,String>("existencia"));

        materiales = FXCollections.observableArrayList();
        tablamateriales.setItems(materiales);

        materialService.getMaterials(materiales);
        System.out.println("Materials: " + materiales.size());
        tablamateriales.refresh();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.inicializarTablaMateriales();
        modificarbt.setDisable(true);
        eliminarbt.setDisable(true);
        final ObservableList<Material> tablaMateriaSel = tablamateriales.getSelectionModel().getSelectedItems();
        tablaMateriaSel.addListener(selectorTablaMateriales);
    }

}