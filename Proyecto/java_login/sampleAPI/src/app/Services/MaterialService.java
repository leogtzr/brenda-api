package app.Services;

import app.Models.Material;
import app.Services.remote.APIService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import javafx.collections.ObservableList;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

public class MaterialService {

    private static String url;
    APIService mAPIService;
    Material material;
    Gson gson = new Gson();

    public MaterialService() {
        this.url = Global.BASE_URI;
        this.mAPIService = Global.getAPIService();
    }

    public void makeMat(String name, String type, String capacity, String stock) {
        final ArrayList<String> arrayList = new ArrayList<String>();

        mAPIService.putMaterial(Global.token, name, type, stock, capacity).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JSONObject errorMsg;
                if (response.isSuccessful()) {
                    material = gson.fromJson(response.body(), Material.class);
                    arrayList.add("OK");
                    System.out.println(arrayList.get(0));
                } else {
                    try {
                        JSONObject errMsg = new JSONObject(response.errorBody().string());
                        System.out.println(errMsg.getString("message"));
                    } catch (JSONException | IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable throwable) {
                System.out.println("Unable to submit post to API.");
            }
        });

    }

    public void remove(final String materialID) {
        mAPIService.delMaterial(Global.token, materialID).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(final Call<JsonObject> call, final Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    material = gson.fromJson(response.body(), Material.class);
                } else {
                    try {
                        final String responseBody = response.errorBody().string();
                        System.out.println(responseBody);
                        System.out.println(materialID);
                    } catch (/*JSONException | */ IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(final Call<JsonObject> call, final Throwable throwable) {
                System.out.println("Unable to submit post to API.");
            }
        });

    }

    public void getMaterials() {
        mAPIService.getMaterials(Global.token).enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(final Call<JsonArray> call, Response<JsonArray> response) {
                if (response.isSuccessful()) {
                    ArrayList<Material> materials = new ArrayList<Material>();
                    for (int i = 0; i < response.body().size(); i++) {
                        materials.add(gson.fromJson(response.body().get(i), Material.class));
                    }
                } else {
                    if (response.code() == 404) {
                        System.out.println("No se encontró.");
                    }
                }
            }

            @Override
            public void onFailure(final Call<JsonArray> call, final Throwable throwable) {

            }
        });
    }

    public void getMaterials(final ObservableList<Material> mats) {
        mAPIService.getMaterials(Global.token).enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(final Call<JsonArray> call, Response<JsonArray> response) {
                if (response.isSuccessful()) {
                    System.out.println("response was successfull ... " + Global.token);
                    for (int i = 0; i < response.body().size(); i++) {
                        final JsonObject jsonObject = gson.fromJson(response.body().get(i), JsonObject.class);
                        final Material material = Material.fromJson(jsonObject);
                        mats.add(material);
                        System.out.println(material + " | " + mats.size());
                    }
                } else {
                    if (response.code() == 404) {
                        System.out.println("No se encontró.");
                    }
                }
            }

            @Override
            public void onFailure(final Call<JsonArray> call, final Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }

    public Optional<JsonObject> editMaterial(
            final String userIdOrTokenId
            , final String materialId
            , final String name
            , final String type
            , final String stock
            , final String capacity
    ) {
        System.out.println("Editing material: " + name);
        Optional<JsonObject> jsonResponse = Optional.empty();
        try {

            /*
            Call<JsonObject> editMaterial(
                @Header("Authorization") String token,
                @Field("name") String name
                , @Field ("type") String type            // not listed in the node API
                , @Field("stock") String stock
                , @Field ("capacity") String capacity     // not listed in the node API
             */

            final Response<JsonObject> editMaterialResponse =
                    mAPIService.editMaterial(userIdOrTokenId, materialId, name, type, stock, capacity).execute();
            System.out.println(editMaterialResponse);
            if (editMaterialResponse.isSuccessful()) {
                jsonResponse = Optional.ofNullable(editMaterialResponse.body());
            } else {
                System.out.println("Response error ");
            }
        } catch (final IOException ex) {
            ex.printStackTrace();
        }
        return jsonResponse;
    }

}

