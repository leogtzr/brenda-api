package app.Services;

import app.Models.Orden;
import app.Models.User;
import app.Services.remote.APIService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import javafx.collections.ObservableList;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class OrdenService {

    private static String url;
    private APIService apiService;

    public OrdenService() {
        this.url = Global.BASE_URI;
        this.apiService = Global.getAPIService();
    }

    public void fillUpOrdersByUser(final ObservableList<Orden> ordenes, final String userIdOrTokenId, final String userId) {
        apiService.getOrdersByUser(userIdOrTokenId, userId).enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(final Call<JsonArray> call, final Response<JsonArray> response) {
                if (response.isSuccessful()) {
                    System.out.println("response was successful ... " + Global.token);
                    for (int i = 0; i < response.body().size(); i++) {
                        final Gson gson = new Gson();
                        final JsonObject jsonObject = gson.fromJson(response.body().get(i), JsonObject.class);
                        System.out.println(jsonObject);
                        final Orden orden = Orden.fromJson(jsonObject);
                        ordenes.add(orden);
                        System.out.println(orden + " | " + ordenes.size());
                    }
                } else {
                    if (response.code() == 404) {
                        System.out.println("No se encontró ... alv" + response.body());
                    } else {
                        System.out.println("Alrpvw> " + response.code());
                    }
                }
            }

            @Override
            public void onFailure(final Call<JsonArray> call, final Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }

    public Optional<JsonObject> markOrderAsDone(final String userIdOrTokenId, final String orderId) {
        System.out.println("Marking order as done ... " + orderId + " , " + userIdOrTokenId);
        Optional<JsonObject> jsonResponse = Optional.empty();
        try {
            final Response<JsonObject> markOrderAsDoneResponse = apiService.markOrderAsDone(userIdOrTokenId, orderId).execute();
            System.out.println(markOrderAsDoneResponse);
            if (markOrderAsDoneResponse.isSuccessful()) {
                jsonResponse = Optional.ofNullable(markOrderAsDoneResponse.body());
            } else {
                System.out.println("Response error ");
            }
        } catch (final IOException ex) {
            ex.printStackTrace();
        }
        return jsonResponse;
    }

}
