package app.Services;

import app.Models.User;
import app.Services.remote.APIService;
import app.utils.UserUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;

public class UserService {

    private static String url;
    APIService mAPIService;
    Gson gson = new Gson();

    public UserService() {
        this.url = Global.BASE_URI; mAPIService = Global.getAPIService();
    }

    public ArrayList<String> syncLog(String x, String y) throws IOException, JSONException {
        JSONObject errorMsg;
        ArrayList<String> res = new ArrayList<String>();

        Response<JsonObject> response = mAPIService.logIn(x, y).execute();
        if (response.isSuccessful()) {
            Global.identity = gson.fromJson(response.body(), User.class);
            res.add("200");
            return res;
        } else {
            errorMsg = new JSONObject(response.errorBody().string());
            res.add("404");
            res.add(errorMsg.getString("message"));
            return res;
        }
    }

    public ArrayList<String> awaitHash(String x, String y) throws IOException, JSONException {
        JSONObject errMsg;
        ArrayList<String> res = new ArrayList<String>();

        Response<JsonObject> response =  mAPIService.getHash(x, y, "123").execute();

        if (response.isSuccessful()) {
            System.out.println("Response when login in: ");
            System.out.println(response.body());
            errMsg = new JSONObject(response.body().toString());
            Global.token = errMsg.getString("token");
            Global.userId = errMsg.getString("userId");
            System.out.println(Global.token);
            res.add("200");
            return res;
        } else {
            errMsg = new JSONObject(response.errorBody().string());
            res.add("404");
            res.add(errMsg.getString("message"));
            System.out.println("Returning: ");
            System.out.println(res);
            return res;
        }
    }

    public void decodeToken() throws Exception {
        JwtService jwtService = new JwtService();
        jwtService.decoded(Global.token);
    }

    public String getUser(final String userIdOrTokenId, final String userId) {

        final app.domain.User user = new app.domain.User();

        mAPIService.getUser(userIdOrTokenId, userId).enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(final Call<JsonObject> call, final Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    System.out.println("response was successful ... " + Global.token);

                    if (response.body().size() > 0) {
                        final Gson gson = new Gson();
                        final JsonObject jsonObject = gson.fromJson(response.body(), JsonObject.class);
                        UserUtils.fillUpUserFromJson(user, jsonObject);
                    }

                } else {
                    if (response.code() == 404) {
                        System.out.println("No se encontró ... alv" + response.body());
                    } else {
                        System.out.println("Alrpvw> " + response.code());
                    }
                }
            }

            @Override
            public void onFailure(final Call<JsonObject> call, final Throwable throwable) {
                throwable.printStackTrace();
            }
        });

        return user.getFullName();
    }



    /*private void makeLog(String x, String y){
        final ArrayList<String> arrayList = new ArrayList<String>();


        mAPIService.logIn(x, y).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JSONObject errorMsg;
                if(response.isSuccessful()){
                    Global.identity = gson.fromJson(response.body(), User.class);
                    arrayList.add("OK");
                }else{
                    try {
                        errorMsg = new JSONObject(response.errorBody().string());
                        arrayList.add(errorMsg.getString("message"));
                        //setDialog(arrayList.get(0), response.code());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable throwable) {
                System.out.println("Unable to submit post to API.");
            }
        });
    }
*/

}
