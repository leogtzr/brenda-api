package app.Services.remote;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import retrofit2.http.*;
import retrofit2.Call;

public interface APIService {

    @POST("admin/login")
    @FormUrlEncoded
    Call<JsonObject> logIn(@Field("username") String username,
                           @Field("password") String password);

    @POST("admin/login")
    @FormUrlEncoded
    Call<JsonObject> getHash(@Field("username") String username,
                             @Field("password") String password,
                             @Field("gethash") String hash);

    @POST("material")
    @FormUrlEncoded
    Call<JsonObject> putMaterial(@Header("Authorization") String token,
                                 @Field("name") String name,
                                 @Field ("type") String type,
                                 @Field("stock") String stock,
                                 @Field ("capacity") String capacity);

    @PUT("materialupdate/{id}")
    @FormUrlEncoded
    Call<JsonObject> editMaterial(
            @Header("Authorization") String token
            , @Path("id") String id
            , @Field("name") String name
            , @Field ("type") String type            // not listed in the node API
            , @Field("stock") String stock
            , @Field ("capacity") String capacity     // not listed in the node API
    );

    @GET("material")
    Call<JsonArray> getMaterials(@Header("Authorization") String token);

    @FormUrlEncoded
    @HTTP(method = "POST", hasBody = true, path = "material/delete")
    Call<JsonObject> delMaterial(@Header("Authorization") String token, @Field("id") String id);


    @DELETE("material")
    Call<JsonArray>dMaterial(@Header("Authorization") String token);

    // TODO: fix this ...
    @GET("userorders/{id}")
    Call<JsonArray> getOrdersByUser(
            @Header("Authorization") String token,
            @Path("id") String id
    );

    @GET("user/{id}")
    Call<JsonObject> getUser(
            @Header("Authorization") String token
            , @Path("id") String id
    );

    @FormUrlEncoded
    @HTTP(method = "POST", hasBody = true, path = "orderdone")
    Call<JsonObject> markOrderAsDone(
            @Header("Authorization") String token
            , @Field("orderId") String orderId
    );

}
