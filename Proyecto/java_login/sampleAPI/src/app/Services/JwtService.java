package app.Services;


import app.Models.User;
import com.google.gson.Gson;
import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import com.sun.org.apache.xml.internal.security.utils.Base64;

import java.io.UnsupportedEncodingException;

public class JwtService {

    Gson gson = new Gson();

    public void decoded(String JWTEncoded) throws Exception {
        try {
            final String[] split = JWTEncoded.split("\\.");
            /*System.out.println("Header: " + getJson(split[0]));
            System.out.println("Body: " + getJson(split[1]));*/
            Global.identity = gson.fromJson(getJson(split[1]), User.class);
        } catch (UnsupportedEncodingException e) {
            //Error
        }
    }

    private static String getJson(String strEncoded) throws UnsupportedEncodingException, Base64DecodingException {
        byte[] decodedBytes = Base64.decode(strEncoded);
        return new String(decodedBytes, "UTF-8");
    }

}
