package app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(final Stage primaryStage) throws Exception {

        // To load a specific view ...
        final Parent root = FXMLLoader.load(getClass().getResource("./Views/LogIn.fxml"));
        //final Parent root = FXMLLoader.load(getClass().getResource("./Views/Ordenes.fxml"));
        // final Parent root = FXMLLoader.load(getClass().getResource("./Views/Menu.fxml"));
        primaryStage.setTitle("Inicia Sesión");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public static void main(final String[] args) {
        launch(args);
    }
}
