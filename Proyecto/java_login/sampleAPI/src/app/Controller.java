package app;

import app.Services.Global;
import app.Services.UserService;
import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Controller {
    @FXML
    JFXButton bt1 = new JFXButton();
    @FXML
    AnchorPane rootPane;
    @FXML
    TextArea messagesArea = new TextArea();
    @FXML
    Label tx1, tx2;

    Date date;


    @FXML
    void initialize() {
        this.messagesArea.clear();
        //tx1.setText(Global.identity.getName());
    }

    @FXML
    private void logIn(ActionEvent event) {
        UserService userService = new UserService();
        try {
            userService.decodeToken();
            date = new Date(Global.identity.getIat()*1000L);
            SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd");
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("h:mm a");
            String formattedDate = dateFormat.format(date);
            String formattedDate1 = dateFormat1.format(date);
            tx2.setText(formattedDate);
            System.out.println(formattedDate + " at " + formattedDate1);

            tx1.setText(Global.identity.getName());

            this.messagesArea.appendText(Global.identity.getName() + " "+ Global.identity.get_id()+"\n");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
