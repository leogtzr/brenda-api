// Creates new order and change the stock based on the materials listed in the practice
// requires: user_id, practice_id
// note: (It fills automatically order's mat list with practice's mat list).
function createOrderChangingStock(req, res) {

    /*
      Algorithm:
      Get the practice materials.
        Locate the materials in the Material model.
          decrease the stock and save

    */

    var order = new Order();
    var params = req.body;

    order.reqDate = moment().unix();
    // TODO: Fix this ...
    // Need to fill up order.user with the actual object, not the param id ...

    User.findOne({_id: params.user}, (err, userFound) => {
        if (err) {
            console.log(err);
        } else {
            console.log('User found: ' + userFound);
            order.user = userFound;
        }
    });

    order.practice = params.practice;
    var materialsWithStockFromPractice = order.practice.material;
    // Iterate the materialsWithStockFromPractice and decrease from the Material model.
    for (var index = 0; index < materialsWithStockFromPractice.length; index++) {
        var materialWithStockInPractice = materialsWithStockFromPractice[index];
        Material.findById(materialWithStockInPractice, (err, materialInDB) => {
            if (err) {
                console.log('Error trying to find: ' + materialWithStockInPractice + ' => ' + err);
                res.status(500).send({message:apiMsg + "2#$#$"});
            } else {
                if (!found) {
                    res.status(500).send({message :"Material no encontrado."});
                } else {
                    // TODO: Change stock here => materialInDB
                    console.log('Material found: ' + materialInDB);
                    console.log('Stock in DB: ' + materialInDB.stock);
                    // TODO: Substract the stock from the materialwithstock
                    materialInDB.stock -= materialWithStockInPractice.stock;

                    console.log('materialInDB has been updated to: ');
                    console.log(materialInDB);
                    // TODO: Save | persist in DB:
                }
            }
        });
    }

    console.log('Creating order for: ' + order.user);
    console.log('With practice: ' + order.practice);

    res.status(200).send(nOrder);

    // TODO:  return here for now ...

    // if (order.practice) {
    //     Practice.findById(order.practice, (err, practice) => {
    //         if (err) {
    //             res.status(500).send({message:apiMsg});
    //         } else {
    //             if (!practice) {
    //                 res.status(404).send({message:"Práctica inexistente."});
    //             }else{
    //                 order.practice = practice;
    //                 var tmp = moment.unix(practice.expDate);
    //                 order.material = practice.material;
    //                 order.expDate = moment(tmp).add(5, 'days').unix();
    //
    //                 // User.findOne({username: order.user}, (err, user) => {
    //                 User.findOne( {$and: [{_id: order.user}]}, (err, found) => {
    //                     if (err) {
    //                         console.log('There was an error ... ');
    //                     }
    //
    //                     if (found) {
    //                         console.log('User found: ' + found);
    //                     } else {
    //                         console.log('User not found .. ');
    //                         // TODO: if the user not found ... should we just return 500?
    //                     }
    //                 });
    //
    //                 // TODO: practice.courses are not tight to the user ... so the following doesn't work ...
    //                 User.findOne({$and: [{_id:order.user} /*, {course: practice.course}*/]}, (err, user) => {
    //                     if (err) {
    //                         res.status(500).send({message:apiMsg});
    //                     } else {
    //                         if (!user) {
    //                             console.log(order.user);
    //                             res.status(404).send({message:"Usuario inexistente."});
    //                         }else{
    //                             order.save((err, nOrder)=>{
    //                                 if(err){
    //                                     res.status(500).send({message:err});
    //                                 }else{
    //                                     if(!nOrder){
    //                                         res.status(404).send({message:"Error al ordenar."});
    //                                     }else{
    //                                         res.status(200).send(nOrder);
    //                                     }
    //                                 }
    //                             });
    //                         }
    //                     }
    //                 });
    //             }
    //         }
    //     });
    // } else {
    //     res.status(400).send({message:"Campos insuficientes."});
    // }
}
